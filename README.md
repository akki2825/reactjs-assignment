## Reactjs assignment

Create an application on top of [Create React App](https://github.com/facebook/create-react-app) that:

- Gets the data from an API (you can go with public APIs)
- Shows a grid of items (thumbnails, cards etc)
- Has a form that allows filtering the dataset
- Shows item details (any information available from the API's response) in a model


### Requirements

#### Must:

- Sufficient test coverage
- Third-party package kept to a minimum
- No external state managers (Redux, etc)

#### Good to have:

- Linter and tests pass
- Uses web fonts
- Code is documented